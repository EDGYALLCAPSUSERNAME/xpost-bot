import re
import time
import praw
import sqlite3
import OAuth2Util

# User config
# --------------------------------------------------------------------
# Don't include the /r/
SOURCE_SUB = ''
POST_SUB = ''

KEYWORDS = []

# --------------------------------------------------------------------


def send_message(r, author):
    subject = 'Your post has been xposted to /r/{}'.format(POST_SUB)
    msg = 'Your post has been xposted.'

    print('Sending message...')
    r.send_message(author, subject, msg, captcha=None)    


def xpost_subm(r, subm, sql, c):
    title = '[/u/{}] {}'.format(subm.author, subm.title)

    print('Submitting post...')
    # submit the post
    r.submit(POST_SUB, title, url=subm.permalink, captcha=None)

    # send the op a message
    send_message(r, subm.author)

    # then add the submission id to db
    c.execute('INSERT INTO posted VALUES(?)', [subm.id])
    sql.commit()  # save the changes


def find_posts(r, sql, c):
    print('Searching for posts...')
    # pattern to match keywords in the title
    pattern = re.compile(r'\b({})\b'.format(r'|'.join(KEYWORDS)), re.IGNORECASE)

    subm_stream = praw.helpers.submission_stream(r, SOURCE_SUB)
    for subm in subm_stream:  # check submissions
        c.execute('SELECT * FROM posted WHERE subm_id = ?', [subm.id])
        if c.fetchone():  # skip the submission if it's already been posted
            continue

        # then search for keywords
        if pattern.search(subm.title):
            xpost_subm(r, subm, sql, c)


def main():
    r = praw.Reddit(user_agent='xpost v1.0 /u/cutety')
    o = OAuth2Util.OAuth2Util(r, print_log=True)

    print('Loading database...')
    # Setup/Load SQL database
    sql = sqlite3.connect('xpost_bot.db')
    c = sql.cursor()

    # Setup schema
    c.execute('CREATE TABLE IF NOT EXISTS posted(subm_id TEXT)')
    sql.commit()  # save the changes

    print('Database loaded.')

    while True:
        try:
            find_posts(r, sql, c)
        except Exception as e:
            print('Error: {}'.format(e))

        print('Sleeping...')
        time.sleep(500)


if __name__ == '__main__':
    main()
